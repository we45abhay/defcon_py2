alert_name = "Application is configured to accept 'none' signed JSON Web Tokens"
alert_desc = "Attacker can create fake tokens to authenticate to the application, using the 'none' signature. The application seems to be authenticate the user based on these 'none' signed tokens. Attackers can use this to bypass authentication and gain deeper access to the application."
alert_cwe = 287
alert_wasc = 1
alert_soln = 'Ensure that JSON Web Tokens are decoded and verified before authenticating and authorizing the user to perform actions on the application'
alert_risk = 3
alert_confidence = 1

dummy_jwt = 'eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJpc3MiOiJodHRwczovL2p3dC1pZHAuZXhhbXBsZS5jb20iLCJzdWIiOiJtYWlsdG86bWlrZUBleGFtcGxlLmNvbSIsIm5iZiI6MTQ5OTg2MDkwNiwiZXhwIjoxNDk5ODY0NTA2LCJpYXQiOjE0OTk4NjA5MDYsImp0aSI6ImlkMTIzNDU2IiwidHlwIjoiaHR0cHM6Ly9leGFtcGxlLmNvbS9yZWdpc3RlciJ9.'

def scanNode(sas, msg):
    orig_msg = msg
    msg = orig_msg.cloneRequest()
    jwt_segments = dummy_jwt.split('.')
    header = jwt_segments[0]
    payload = jwt_segments[1]
    signature = jwt_segments[2]
    if orig_msg.getRequestHeader().getHeader('Authorization'):
        print "JWT: ", header, payload, signature
        msg.getRequestHeader().setHeader('Authorization', dummy_jwt)
        sas.sendAndReceive(msg, False, False)
        status_code = msg.getResponseHeader().getStatusCode()
        if status_code == 200:
            sas.raiseAlert(alert_risk, alert_confidence, alert_name, alert_desc,
                           msg.getRequestHeader().getURI().toString(), dummy_jwt, '', '',
                           alert_soln, '',
                           alert_cwe, alert_wasc, msg)




def scan(sas, msg, param, value):
    pass