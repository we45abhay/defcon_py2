import requests
import urllib2
import threading
import time
from mitm_jwt_script import JWTTokenValidator
from mitmproxy import controller, proxy
from mitmproxy.proxy.server import ProxyServer
# from jwt_bruteforce import jwt_bruteforce_attack
import json

app_proxyhost = '127.0.0.1'
proxyport = 8080
app_port = 5050
active_queues = []

proxy_dict = {
    'http': 'http://localhost:8085',
    'https': 'http://localhost:8085'
}

login_url = 'http://{0}:{1}/login'.format(app_proxyhost, app_port)
auth_json = {'username': 'admin', 'password': 'admin123'}


class MitmThread(threading.Thread):
    def run(self):
        config = proxy.ProxyConfig(port=8085)
        server = ProxyServer(config)
        self.mitm_session = JWTTokenValidator(server)
        self.mitm_session.run()


    def write_to_file(self):
        self.mitm_session.write_to_file()

    def stop_proxy(self):
        self.mitm_session.kill_proxy()

    def bruteforce(self):
        self.mitm_session.jwt_bruteforce_attack()

class APIRequester(threading.Thread):
    def run(self):
        resp = requests.post(login_url, json = auth_json, proxies = proxy_dict)
        print "session is going to be closed now...."
        resp.close()


if __name__ == '__main__':
    print("Starting MITMProxy Thread now....")
    mitm_thread = MitmThread()
    mitm_thread.start()
    time.sleep(50)
    print("Running All API Requests now...")
    req_thread = APIRequester()
    req_thread.start()
    while req_thread.is_alive():
        pass
    else:
        mitm_thread.write_to_file()
        mitm_thread.stop_proxy()
        mitm_thread.bruteforce()
        # brutethread = JWTBruteforceThread()
        # brutethread.start()