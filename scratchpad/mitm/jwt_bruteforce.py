import jwt
from jwt.exceptions import DecodeError, ExpiredSignatureError
import json
passfile = 'common_pass.txt'
tokenfile = 'tokenfile.json'


def jwt_bruteforce_attack():
    print "Loading Single Token now..."
    with open(tokenfile,'r') as tkfile:
        all_tokens = json.loads(tkfile.read())
        for token_dict in all_tokens:
            with open(passfile, 'r') as pfile:
                for line in pfile:
                    if line:
                        try:
                            #print token_dict['token'], line.strip()
                            decoded = jwt.decode(token_dict['token'], line.strip(), algorithms=['HS256'], verify_exp = False)
                            print "Decoded", decoded, "Key: ", line.strip()
                            raise_alert(token_dict['url'], token_dict['token'], line.strip())
                            break
                        except DecodeError:
                            #print("INVALID JWT Bruteforce attack with '{0}'".format(line.rstrip()))
                            pass
                        except ExpiredSignatureError:
                            pass


def raise_alert(url, token, evidence):
    vul_dict = {'name': 'Weak HMAC Secret Key for JSON Web Token',
                'description': 'The Scanner was able to perform a bruteforcing attack against the HMAC-SHA-256 Secret Key used to perform integrity verification of the JWT. With the knowledge of the shared secret, attackers would be able to forge tokens and possibly compromise user accounts',
                'cwe': 261,
                'owasp': 'Broken Authentication and Session Management',
                'solution': 'Ensure that strong secrets are used for the HMAC-SHA-256 Operation on the JSON Web Token',
                'evidence': evidence,
                'param': token,
                'url': url
                }
    print vul_dict


# jwt_bruteforce_attack()