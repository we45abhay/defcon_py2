from mitmproxy import controller, proxy
from mitmproxy.proxy.server import ProxyServer
# from mitmproxy.models import decoded
import json
import os

passfile = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'common_pass.txt')
tokenfile = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'tokenfile.json')
import jwt
from jwt.exceptions import DecodeError, ExpiredSignatureError
from tinydb import TinyDB
from uuid import uuid4
import os

class JWTTokenValidator(controller.Master):
    def __init__(self, server):
        controller.Master.__init__(self, server)
        self.token_map = []
        self.outfile = 'tokenfile.json'
        self.jwt_bruteforce_name = 'Weak HMAC Secret Key for JSON Web Token'
        self.jwt_bruteforce_desc = 'The Scanner was able to perform a bruteforcing attack against the HMAC-SHA-256 Secret Key used to perform integrity verification of the JWT. With the knowledge of the shared secret, attackers would be able to forge tokens and possibly compromise user accounts'
        self.jwt_bruteforce_cwe = 261
        self.jwt_bruteforce_owasp = 'Broken Authentication and Session Management'
        self.jwt_bruteforce_soln = 'Ensure that strong secrets are used for the HMAC-SHA-256 Operation on the JSON Web Token'


    def run(self):
        try:
            print("Running MitmProxy Session....")
            controller.Master.run(self)
        except KeyboardInterrupt:
            print("Shutting down thread....")
            self.write_to_file()
            self.shutdown()


    def handle_response(self, flow):
        all_headers = flow.response.headers
        if all_headers:
            for header, value in all_headers.iteritems():
                if "Authorization" in header:
                    print "Found Authorization Token in HTTP Response Header for url {0}".format(flow.request.url)
                    print "Token: ", all_headers[header]
                    self.token_map.append({'url': flow.request.url, 'token': all_headers[header]})
        flow.reply()


    def raise_alert(self,name, desc, cwe, owasp, soln, url, param, evidence,attack):
        scan_id = unicode(uuid4())
        vul_dict = {'name': name,
                    'description': desc,
                    'cwe': cwe,
                    'owasp': owasp,
                    'solution': soln,
                    'evidence': evidence,
                    'param': param,
                    'url': url,
                    'scan': scan_id,
                    'attack': attack,
                    'tool': 'mitmproxy'
                    }
        db = TinyDB(os.environ.get('VUL_DB', '/root/Desktop/vul_db.json'))
        db.insert(vul_dict)
        print("Inserted into DB")

    def jwt_bruteforce_attack(self):
        print "Loading Single Token now..."
        with open(tokenfile, 'r') as tkfile:
            all_tokens = json.loads(tkfile.read())
            for token_dict in all_tokens:
                with open(passfile, 'r') as pfile:
                    for line in pfile:
                        if line:
                            try:
                                # print token_dict['token'], line.strip()
                                decoded = jwt.decode(token_dict['token'], line.strip(), algorithms=['HS256'], verify_exp=False)
                                print "Decoded", decoded, "Key: ", line.strip()
                                self.raise_alert(self.jwt_bruteforce_name, self.jwt_bruteforce_desc, self.jwt_bruteforce_cwe, self.jwt_bruteforce_owasp, self.jwt_bruteforce_soln, token_dict['url'], token_dict['token'], line.strip(), line.strip())
                                break
                            except DecodeError:
                                print("INVALID JWT Bruteforce attack with '{0}'".format(line.rstrip()))
                                pass
                            except ExpiredSignatureError:
                                pass



    def write_to_file(self):
        try:
            with open(self.outfile, 'w') as jsonfile:
                jsonfile.write(json.dumps(self.token_map))
        except:
            print("Error has occurred in writing Token Map JSON File")
            self.shutdown()

    def kill_proxy(self):
        self.write_to_file()
        self.shutdown()



# config  = proxy.ProxyConfig(port=8080)
# server = ProxyServer(config)
# jwt_token_validator = JWTTokenValidator(server)
# jwt_token_validator.run()