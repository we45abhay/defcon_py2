'''

The objective of this exercise is to attempt to use an existing ZAP session to create a context and launch a scan in an automated manner.

'''


from zapv2 import ZAPv2 as ZAP
import subprocess
import os
import time
import base64
from shutil import copy
from uuid import uuid4
import json
import sys
from tinydb import TinyDB
reload(sys)
sys.setdefaultencoding('UTF8')

settings_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config_session_context.json')
#parse and load settings
with open(settings_file, 'rb') as config:
    json_val = config.read()

config_dict = json.loads(json_val)

db = TinyDB(os.environ.get('VUL_DB', '/root/Desktop/vul_db.json'))

target_regex = "{0}.*".format(config_dict['target_app'])


def load_session():
    curdir = os.path.dirname(os.path.realpath(__file__))
    for single_file in config_dict['del_files']:
        get_full_path = os.path.join(curdir, single_file)
        if os.path.exists(get_full_path):
            print "Existing Session Files in Context Directory. Removing..."
            os.remove(get_full_path)


    fresh_directory = "{0}/{1}".format(os.path.dirname(os.path.realpath(__file__)), 'fresh')
    for copy_file in os.listdir(fresh_directory):
        full_file_name = os.path.join(fresh_directory, copy_file)
        if (os.path.isfile(full_file_name)):
            copy(full_file_name, os.path.dirname(os.path.realpath(__file__)))


# #immutables
load_session()
ZAP_PATH = config_dict['zap_path_prod']

print("[ + ] Starting ZAP. Please wait....")
cmd = '{0} -config api.disablekey=true -port 8090'.format(ZAP_PATH)
subprocess.Popen(cmd.split(' '), stdout=open(os.devnull, 'w'))
print("[ + ] Waiting for ZAP to initialize....")
time.sleep(60)
zap = ZAP(proxies= {
    'https': 'http://localhost:8090',
    'http': 'http://localhost:8090',
})
time.sleep(5)

zap.core.load_session(os.path.join(os.path.dirname(os.path.realpath(__file__)), config_dict['session_file']))
time.sleep(4)
context = zap.context.new_context(config_dict['context_name'])
zap.context.include_in_context(config_dict['context_name'], target_regex)
time.sleep(2)
zap.script.disable(config_dict['exclude_from_policy'])
zap.ascan.scan(contextid=context, scanpolicyname=config_dict['scan_policy'])
time.sleep(5)
while int(zap.ascan.status()) < 100:
    print("[ + ] Scanning site at {0}%".format(zap.ascan.status()))
    time.sleep(5)

core = zap.core

scan = unicode(uuid4())

for i, na in enumerate(core.alerts()):
    vul = {}
    vul['app'] = config_dict['target_app_name']
    vul['scan'] = scan
    vul['name'] = na['alert']
    vul['policy'] = config_dict['scan_policy']
    vul['tool'] = "ZAP"
    vul['confidence'] = na.get('confidence', '')
    if na.get('risk') == 'High':
        vul['severity'] = 3
    elif na.get('risk') == 'Medium':
        vul['severity'] = 2
    elif na.get('risk') == 'Low':
        vul['severity'] = 1
    else:
        vul['severity'] = 0

    vul['cwe'] = na.get('cweid', 0)
    vul['uri'] = na.get('url', '')
    vul['param'] = na.get('param', '')
    vul['attack'] = na.get('attack', '')
    vul['evidence'] = na.get('evidence', '')
    message_id = na.get('messageId', '')
    message = core.message(message_id)
    if isinstance(message, dict):
        request = base64.b64encode("{0}{1}".format(message['requestHeader'], message['requestBody']))
        response = base64.b64encode("{0}{1}".format(message['responseHeader'], message['responseBody']))
        vul['request'] = request
        vul['response'] = response
        vul['rtt'] = int(message['rtt'])

    if vul:
        db = TinyDB(os.environ.get('VUL_DB', '/root/Desktop/vul_db.json'))
        db.insert(vul)



zap.context.remove_context(config_dict['context_name'])
zap.core.shutdown()
#
