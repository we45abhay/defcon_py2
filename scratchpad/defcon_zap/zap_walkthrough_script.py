from zapv2 import ZAPv2 as ZAP
import subprocess
import os
import time
import base64
from uuid import uuid4
import json
import sys
from tinydb import TinyDB
reload(sys)
sys.setdefaultencoding('UTF8')
from WeCareAuthScript import WeCareAuthScript

#configs are loaded from the json file
settings_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config_session_context.json')
#parse and load settings
with open(settings_file, 'rb') as config:
    json_val = config.read()

config_dict = json.loads(json_val)

target_regex = "{0}.*".format(config_dict['target_app'])

#setting the path to launch ZAP 2.6.0 from
ZAP_PATH = config_dict['zap_path_prod']


print("[ + ] Starting ZAP. Please wait....")
cmd = '{0} -config api.disablekey=true -port 8090'.format(ZAP_PATH)
subprocess.Popen(cmd.split(' '), stdout=open(os.devnull, 'w'))
print("[ + ] Waiting for ZAP to initialize....")
time.sleep(50)

#ZAP object is being initialized at localhost, port 8090
zap = ZAP(proxies= {
    'https': 'http://localhost:8090',
    'http': 'http://localhost:8090',
})
time.sleep(5)

w = WeCareAuthScript(proxy_host = 'localhost', proxy_port = '8090', target = config_dict['target_app'])
w.run_script()
time.sleep(10)
zap.ascan.add_scan_policy(scanpolicyname=config_dict['scan_policy'])
zap.ascan.scan(config_dict['target_app'])

time.sleep(5)

while int(zap.ascan.status()) < 100:
    print("[ + ] Scanning site at {0}%".format(zap.ascan.status()))
    time.sleep(5)

core = zap.core
scan = unicode(uuid4())

for i, na in enumerate(core.alerts()):
    vul = {}
    vul['app'] = config_dict['target_app_name']
    vul['scan'] = scan
    vul['name'] = na['alert']
    vul['policy'] = config_dict['scan_policy']
    vul['tool'] = "ZAP"
    vul['confidence'] = na.get('confidence', '')
    if na.get('risk') == 'High':
        vul['severity'] = 3
    elif na.get('risk') == 'Medium':
        vul['severity'] = 2
    elif na.get('risk') == 'Low':
        vul['severity'] = 1
    else:
        vul['severity'] = 0

    vul['cwe'] = na.get('cweid', 0)
    vul['uri'] = na.get('url', '')
    vul['param'] = na.get('param', '')
    vul['attack'] = na.get('attack', '')
    vul['evidence'] = na.get('evidence', '')
    message_id = na.get('messageId', '')
    message = core.message(message_id)
    if isinstance(message, dict):
        request = base64.b64encode("{0}{1}".format(message['requestHeader'], message['requestBody']))
        response = base64.b64encode("{0}{1}".format(message['responseHeader'], message['responseBody']))
        vul['request'] = request
        vul['response'] = response
        vul['rtt'] = int(message['rtt'])

    if vul:
        db = TinyDB(os.environ.get('VUL_DB', '/root/Desktop/vul_db.json'))
        db.insert(vul)

#Removing Context and Shutting down scanner
# zap.context.remove_context(config_dict['context_name'])
zap.core.shutdown()