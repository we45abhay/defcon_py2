from tinydb import TinyDB, Query

db_file = '/Users/abhaybhargav/Documents/Code/Python/defcon_py2/scratchpad/vul_db.json'

def open_connection():
    try:
        db = TinyDB(db_file)
        return db
    except Exception as e:
        print("Error in accessing DB", str(e.message))

def agg_by_cwe():
    cwe_dict = {}
    try:
        db = open_connection()
        for val in iter(db):
            if val['cwe']:
                cwe_val = val['cwe']
                if cwe_val in cwe_dict.keys():
                    cwe_dict[cwe_val] = cwe_dict[cwe_val] + 1
                else:
                    cwe_dict[cwe_val] = 1
        return cwe_dict
    except Exception as e:
        print 'Unable to perform CWE Aggregation', e.message